/*
    invoke specific callback function on condition..
    (integration)
*/
// function(callback){
function scan(arr, i=0, r=[]){
    
    //console.log(i); i want contents
    if(typeof arr[i]=="object"){
      if(Array.isArray(arr[i])){
        console.log("ArrayHandle",arr,"i",arr[i]) // a haaa 2
        r.push(scan(arr[i])) // <-- ins array copy(recursive)
      }else{
        console.log("ObjectHandle","i",arr[i])
        console.log(1,arr[i])
        r.push(iterationCopy(arr[i])) // <-- ins object copy(iteration)
      }
    }else if(typeof arr[i]!=="undefined"){
        console.log("PrimitiveHandle",arr[i],"i",arr[i]) // <--  primitive(just the value)
        r.push(arr[i]);
    }
    
    if(typeof arr[i+1]!=="undefined"){
      scan(arr,i+1,r); // route
    }

    // helper function for copying an object
    function iterationCopy(src) {
      let target = {};
      for (let prop in src) {
          if (src.hasOwnProperty(prop)) {
              if(typeof src[prop]=="object"){
                  if(Array.isArray(src[prop])){
                      console.log("ArrayHandle",src[prop])
                      target[prop] = scan(src[prop]);
                  }else{
                        console.log("ObjectHandle", src[prop])
                      target[prop] = iterationCopy(src[prop]);
                  }
              }else{
                  target[prop] = src[prop];
              }
          }
      }
      return target;
    }
    return r;
  }

  scan([1,[2],"3",{a:4,b:43}])